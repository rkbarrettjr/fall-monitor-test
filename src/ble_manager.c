/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_ble_manager ble_manager.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief BLE Manager
 *
 * This file contains functions that initialize the BLE stack in
 * Central mode. It also scans for BLE peripherals and manages the 
 * connections made to discovered devices.
 */
#include "ble_manager.h"
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <string.h>
#include "logging/log.h"
#include "device_manager.h"
#include "state_machine.h"

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL); 

/** @brief BLE custom service UUID */
#define 								TIDI_SVC_UUID   0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x00, 0x20, 0x0F, 0xF1
/** @brief BLE control characteristic */
#define 								TIDI_CTRL_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x01, 0x20, 0x0F, 0xF1
/** @brief BLE notification characteristic */
#define 								TIDI_INFO_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x02, 0x20, 0x0F, 0xF1

#define									MAX_INT_INFO 5									/**< max number entries in a local interrupt info list */
static struct bt_conn 					*m_establishing_connection;						/**< placeholder for connection while it is establihing */
static struct bt_uuid_16 				m_uuid16 = BT_UUID_INIT_16(0);					/**< 16 bit UUID buffer */
static struct bt_uuid_128 				m_uuid128 = BT_UUID_INIT_128(0);				/**< 128 bit UUID buffer */
static struct bt_gatt_discover_params 	m_discover_params;								/**< UUID discover params buffer */
static struct bt_gatt_subscribe_params 	m_subscribe_params;								/**< UUID subscribe params buffer */
static struct bt_gatt_read_params 		m_read_params;									/**< UUID reading buffer */
static struct bt_gatt_write_params 		m_write_params;									/**< UUID writing buffer */
static uint8_t 							m_num_connected_devs = 0;						/**< tracks number of connected devices */
static uint16_t 						m_ctrl_characteristic_handle;					/**< temp storage for write/read control UUID */
static char								m_scan_name[DEVICE_NAME_LEN + 1];				/**< buffer for device name being scanned */
static bool								m_scan_enabled = false;							/**< tracks BLE scan state */
static ble_device_info_t				m_ble_int_info[MAX_INT_INFO];					/**< storage for BLE interrupt list */
static int								m_ble_int_info_index = 0;						/**< tracks current entry in interrupt list */

//local helper functions
static void connected(struct bt_conn *conn, uint8_t conn_err);
static void disconnected(struct bt_conn *conn, uint8_t reason) ;
static uint8_t notify_func(struct bt_conn *conn,struct bt_gatt_subscribe_params *params,
			   const void *data, uint16_t length);
static uint8_t read_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_read_params *params, 
                const void *data, uint16_t length);
static void write_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_write_params *params);
static uint8_t discover_func(struct bt_conn *conn, const struct bt_gatt_attr *attr, 
				struct bt_gatt_discover_params *params);
static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
			 	struct net_buf_simple *ad);
static struct bt_conn *get_device_connection(char *device_name);

//callbacks for BLE connect / disconnect interrupts
static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

/** @brief BLE Initialization
 * 
 * @return 0 on success
 */
int ble_mgr_init(void) {
	LOG_DBG("Begin BLE Manager initialization...");

	//initialize manager to handle BLE device connections
	device_manager_init();

	//enable the BLE stack
	int err = bt_enable(NULL);
	if (err == 0) {
		//register the connect and disconnect callback functions
		bt_conn_cb_register(&conn_callbacks);
	}
	return err;
}

/** @brief Start BLE Scan
 * 
 * @details Starts a scan for a device advertising the provided name.
 * 			The name being scanned is obtained via an NFC read of the
 * 			device's NFC tag.
 * 
 * @param[in] device_name name of device to scan for
 * @return 0 on success
 */
int ble_mgr_start_scan(char *device_name) {
	int err;
	//if already scanning for the device
	if (m_scan_enabled) {
		//if same device name
		if (!memcmp(device_name, m_scan_name, DEVICE_NAME_LEN)) {
			err = 0;
		} else {
			//scanning but not for requested device, return error
			err = 1;
		}
	} else {
		m_scan_enabled = true;
		//save the advertised name we are scanning for
		memcpy(m_scan_name, device_name, DEVICE_NAME_LEN);
		m_scan_name[16] = '\0';
		//printk("Begin scanning for device: %s\n", m_scan_name);
		LOG_DBG("Begin scanning for device: %s", log_strdup(m_scan_name));

		/* Use active scanning and disable duplicate filtering to handle any
		* devices that might update their advertising data at runtime. */
		struct bt_le_scan_param scan_param = {
			.type       = BT_LE_SCAN_TYPE_ACTIVE,
			.options    = BT_LE_SCAN_OPT_FILTER_DUPLICATE,
			.interval   = BT_GAP_SCAN_FAST_INTERVAL,
			.window     = BT_GAP_SCAN_FAST_WINDOW,
		};

		err = bt_le_scan_start(&scan_param, device_found);
		if (err) {
			LOG_ERR("Scanning failed to start (err %d)", err);
		} else {
			LOG_DBG("Scanning successfully started");
		}
	}
	return err;
}

/** @brief Stop BLE Scan
 * 
 * @return 0 on success
 */
int ble_mgr_stop_scan(void) {
	int err = 0;
	if (m_scan_enabled) {
		m_scan_enabled = false;
		err = bt_le_scan_stop();
		if (err) {
			LOG_ERR("Stop LE scan failed (err %d)", err);
		}
	}
	return err;
}

/** @brief BLE Connect Device
 * 
 * @details Connects the device with the provided address.
 * 
 * @param[in] ble_addr Bluetooth address of device
 * @return 0 on success
 */
int ble_mgr_connect_device(bt_addr_le_t *ble_addr) {
	LOG_DBG("Connecting device");
	struct bt_le_conn_param *param = BT_LE_CONN_PARAM_DEFAULT;
	int err = bt_conn_le_create(ble_addr, BT_CONN_LE_CREATE_CONN,
		param, &m_establishing_connection);
	if (err) {
		LOG_ERR("Error (%d) connecting", err);
	}
	return err;
}

/** @brief BLE Disconnect Device
 * 
 * @details Disconnects the device with the given address.
 * 
 * @param[in] ble_addr Bluetooth address of device
 * @return 0 on success
 */
int ble_mgr_disconnect_device(bt_addr_le_t *ble_addr) {
	LOG_DBG("Disconnecting device");
	struct bt_conn *conn = bt_conn_lookup_addr_le(BT_ID_DEFAULT, ble_addr);
	if (conn == NULL) {
		LOG_WRN("Connection NULL - device already disconnected");
		return 0;
	}
	int err = bt_conn_disconnect(conn, 0);
	bt_conn_unref(conn);
	if (err) {
		LOG_ERR("Error (%d) disconnecting", err);
	}
	return err;
}

/** @brief BLE Write to Peripheral
 * 
 * @details Writes the provided data to the device using the control
 * 			characteristic. This is the function that allows the 
 * 			Fall Monitor to send commands to the peripheral devices.
 * 
 * @param[in] device_name name of device to write to
 * @param[in] data data to write 
 * @return 0 on success
 */
int ble_write_device(char *device_name, uint32_t data) {
		const bt_addr_le_t *ble_addr = dm_get_device_addr(device_name);
		struct bt_conn *conn;
		if (ble_addr != NULL) {
			conn = bt_conn_lookup_addr_le(BT_ID_DEFAULT, ble_addr);
			if (conn == NULL) {
				LOG_ERR("No Connection found on ble write");
				return 1;
			}
		} else {
			LOG_ERR("No BLE Address found on ble write");
			return 1;
		}
		
		//write to the UUID characteristic
		m_write_params.data = &data;
		m_write_params.length = sizeof(uint32_t);
		m_write_params.handle = m_ctrl_characteristic_handle;
		m_write_params.offset = 0;
		m_write_params.func = write_cb;
		return bt_gatt_write(conn, &m_write_params);
}

/** @brief BLE Read from Peripheral
 * 
 * @details Reads data from the device using the control
 * 			characteristic.
 * 
 * @param[in] device_name name of device to read from
 * @return 0 on success
 */
int ble_read_device(char *device_name) {
	int err = 0;
	struct bt_conn *conn = get_device_connection(device_name);
	if (conn != NULL) {
		m_read_params.func = read_cb;
		m_read_params.handle_count = 1;
		m_read_params.single.handle = m_ctrl_characteristic_handle;
		m_read_params.single.offset = 0;
		bt_gatt_read(conn, &m_read_params);
	} else {
		LOG_ERR("Device BLE Connection not found");
		err = 1;
	}
	return err;
}

//find the BT connection associated with the provide name
static struct bt_conn *get_device_connection(char *device_name) {
	const bt_addr_le_t *ble_addr = dm_get_device_addr(device_name);
	struct bt_conn *conn = NULL;
	if (ble_addr != NULL) {
		conn = bt_conn_lookup_addr_le(BT_ID_DEFAULT, ble_addr);
	} 
	return conn;
}

//BLE extended inquiry response - if name in response matches the scan name indicate dev found
static bool eir_found(struct bt_data *data, void *user_data) {
	if (data->type == BT_DATA_NAME_COMPLETE) {
		memcpy(m_ble_int_info[m_ble_int_info_index].device_name, data->data, DEVICE_NAME_LEN);
		//if advertised name matches scan namethen connect
		if (!memcmp(m_scan_name, m_ble_int_info[m_ble_int_info_index].device_name, DEVICE_NAME_LEN)) {
			//printk("BLE Scan found device: %s\n", m_scan_name);
			LOG_DBG("BLE Scan found device: %s", log_strdup(m_scan_name));
			ble_mgr_stop_scan();

			//generate state machine event
			memcpy(&m_ble_int_info[m_ble_int_info_index].addr, user_data, sizeof(bt_addr_le_t));
			sm_create_event(BLE_DEV_FOUND_EVT, &m_ble_int_info[m_ble_int_info_index++]);
			if (m_ble_int_info_index >= MAX_INT_INFO) {
				m_ble_int_info_index = 0;
			}
		}
	}
	LOG_DBG("EIR data type: %d", data->type);
	return true;
}

//call back from BLE Scan when a BLE device is found - dev name is matched in EIR function
static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
			 struct net_buf_simple *ad) {

	//copy just the BLE address (00:11:22:33:44:55)
	char dev_addr[BT_ADDR_LE_STR_LEN];
	bt_addr_le_to_str(addr, dev_addr, 18);

	//only interested in connectable events
	if (type == BT_GAP_ADV_TYPE_ADV_IND ||
	    type == BT_GAP_ADV_TYPE_ADV_DIRECT_IND) {
		LOG_DBG("[DEVICE]: %s, AD evt type %u, AD data len %u, RSSI %i",
			log_strdup(dev_addr), type, ad->len, rssi);
		// printk("[DEVICE]: %s, AD evt type %u, AD data len %u, RSSI %i\n",
	    //    	dev_addr, type, ad->len, rssi);
		
		//parse the Extended Inquiry Response 
		bt_data_parse(ad, eir_found, (void *)addr);
	}
}

//callback when device makes a connection
static void connected(struct bt_conn *conn, uint8_t conn_err) {
	char addr[BT_ADDR_LE_STR_LEN];
	int err;

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (conn_err) {
		//printk("Failed to connect to %s (%u)\n", addr, conn_err);
		LOG_ERR("Failed to connect to %s (%u)", log_strdup(addr), conn_err);
		bt_conn_unref(conn);
		return;
	};

	m_num_connected_devs++;
	//printk("Connected: %s (Total devices connected: %d)\n", addr, m_num_connected_devs);
	LOG_DBG("Connected: %s (Total devices connected: %d)", log_strdup(addr), m_num_connected_devs);
	
	if (conn == m_establishing_connection) {
		//start discovery of the TIDI service
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_SVC_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.func = discover_func;
		m_discover_params.start_handle = 0x0001;
		m_discover_params.end_handle = 0xffff;
		m_discover_params.type = BT_GATT_DISCOVER_PRIMARY;

		//discovered devices will be handled in the discover_func() callback
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			LOG_ERR("Immediate discover failed(err %d)", err);
			return;
		}
	}
}

//callback when device disconnects
static void disconnected(struct bt_conn *conn, uint8_t reason) {
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	m_num_connected_devs--;
	//printk("Disconnected: %s (reason 0x%02x) (Total devices connected: %d)\n", addr, reason, m_num_connected_devs);
	LOG_DBG("Disconnected: %s (reason 0x%02x) (Total devices connected: %d)", log_strdup(addr), reason, m_num_connected_devs);
	sm_create_event(BLE_DISCO_EVT, dm_get_device_info(conn));

	bt_conn_unref(conn); //TODO is this needed here? 
}

//callback function for BLE notifications
static uint8_t notify_func(struct bt_conn *conn,
			   struct bt_gatt_subscribe_params *params,
			   const void *data, uint16_t length) {
	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0U;
		return BT_GATT_ITER_STOP;
	}
	device_info_t *info = dm_get_device_info(conn);
	//copy the notification payload and generate state machine event
	memcpy(info->notification, data, length);
	sm_create_event(BLE_NOTIF_EVT, info);

	return BT_GATT_ITER_CONTINUE;
}

//callback for reading
static uint8_t read_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_read_params *params, const void *data, uint16_t length) {
	//TODO - not sure if we even need this
	uint8_t *tmp = (uint8_t *)data;
	printk("Read Data: ");
	for (int i = 0; i < length; i++) {
		printk("%x ", tmp[i]);
	}
	printk("\n");
	return 0;
}

//callback for writing
static void write_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_write_params *params) {
	//TODO ???
	LOG_DBG("Got write callback: %d", err);
}

//call back function when services, characteristics, and descriptors are discovered
static uint8_t discover_func(struct bt_conn *conn, const struct bt_gatt_attr *attr, 
								struct bt_gatt_discover_params *params) {
	int err;
	if (!attr) {
		LOG_WRN("Discover attr is NULL");
		(void)memset(params, 0, sizeof(*params));
		return BT_GATT_ITER_STOP;
	}
	LOG_DBG("[ATTRIBUTE] handle %u", attr->handle);

	//discover services, characteristics, and descriptors
	if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_SVC_UUID))) {
		LOG_DBG("Discovered the TIDI Service");
		//discover the Control characteristic next
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_CTRL_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;
		m_discover_params.start_handle = attr->handle + 1;
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			LOG_ERR("Discover failed (err %d)", err);
		}
	} else if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_CTRL_UUID))) {
		LOG_DBG("Discovered the TIDI Control Characteristic");
		//discover the Info chacteristic next
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_INFO_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;
		m_discover_params.start_handle = attr->handle + 1;
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			LOG_ERR("Discover failed (err %d)", err);
		}
		//save handle for this characteristic - used for read/write of this characteristic
		m_ctrl_characteristic_handle = attr->handle+1;
	} else if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_INFO_UUID))) {
		LOG_DBG("Discovered the TIDI Info Characteristic");
		//discover the CCC descriptor next
		memcpy(&m_uuid16, BT_UUID_GATT_CCC, sizeof(m_uuid16));
		m_discover_params.uuid = &m_uuid16.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_DESCRIPTOR;
		m_discover_params.start_handle = attr->handle + 1;
		//save the handle of the Info characteristic attribute
		m_subscribe_params.value_handle = bt_gatt_attr_value_handle(attr);
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			LOG_ERR("Discover failed (err %d)", err);
		}
	} else {
		LOG_DBG("Discover GATT CCC Descriptor");
		//subscribe to the notification
		m_subscribe_params.notify = notify_func;
		m_subscribe_params.value = BT_GATT_CCC_NOTIFY;
		m_subscribe_params.ccc_handle = attr->handle;

		err = bt_gatt_subscribe(conn, &m_subscribe_params);
		if (err && err != -EALREADY) {
			LOG_ERR("Subscribe failed (err %d)", err);
		} else {
			LOG_DBG("[SUBSCRIBED]");
			//device is now available for operation in the Fall Monitor system, indicate connected
			sm_create_event(BLE_CONN_EVT, dm_get_device_info(conn));
		}
	}
	return BT_GATT_ITER_STOP;
}

/**
 * @}
 */