/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_nfc_intf_manager nfc_intf_manager.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief NFC Interface Manager
 *
 * This file contains functions that manager the NFC's I2C bus, the GPIO
 * interrupt, and retrieving information from the NFC reader.
 */
#include "nfc_intf_manager.h"
#include <logging/log.h>
#include "state_machine.h"

#include "buttons.h" //TODO REMOVE

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL);   

#define     NUM_DEV_DATA    5
static bool m_nfc_scan_enabled = false;
static char m_nfc_device_data[NUM_DEV_DATA][DEVICE_NAME_LEN];
static int  m_nfc_device_data_index = 0;

int nfc_intf_mgr_init(void) {
    LOG_DBG("Begin NFC Interface initialization...");
    init_buttons(); //TODO remove
    for (int i = 0; i < NUM_DEV_DATA; i++) {
        memset(m_nfc_device_data[i], 0, DEVICE_NAME_LEN);
    }
    return 0;
}
void nfc_start_scan(void) {
    LOG_DBG("NFC Scan Enabled");
    m_nfc_scan_enabled = true;
}

void nfc_stop_scan(void) {
    LOG_DBG("NFC Scan Disabled");
    m_nfc_scan_enabled = false;

}

//TODO REMOVE
void simulate_nfc_read(int num) {
    LOG_DBG("Simulating NFC Read");
    if (num == 1) {
        memcpy(m_nfc_device_data[m_nfc_device_data_index], "NCS-C2E7DAA583C7", DEVICE_NAME_LEN);
        //memcpy(m_nfc_device_data[m_nfc_device_data_index], "NCS-FB8DB9CBDF15", DEVICE_NAME_LEN);
        sm_create_event(NFC_READ_EVT, m_nfc_device_data[m_nfc_device_data_index++]);
        if (m_nfc_device_data_index >= NUM_DEV_DATA) {
            m_nfc_device_data_index = 0;
        }
    }
}

/**
 * @}
 */