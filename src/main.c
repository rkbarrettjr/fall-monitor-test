/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_main main.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief Sitter on Cue main file.
 *
 * This file contains main() for the Sitter on Cue NRF52 hardware. main() will initialize the
 * components of the Fall Monitor system before entering its primary loop.
 */
#include <zephyr.h>
#include <logging/log.h>
#include "ble_manager.h"
#include "gpio_util.h"
#include "mcu_intf_manager.h"
#include "nfc_intf_manager.h"
#include "state_machine.h"
#include "work_queue_util.h"

/** @brief Register logger for Fall Monitor */
LOG_MODULE_REGISTER(FallMonNrf, CONFIG_LOG_MAX_LEVEL);

//Error loop main enters on any component failure to initialize
static void error_idle_loop(void) {
    while (1) {
        k_sleep(K_SECONDS(1));
    }
}

/** @brief Main for Fall Monitor */
void main(void) {
    int err;
    LOG_DBG("Begin SoC Fall Monitor initialization...");

    //initialize work queue to offload interrupt handling to lower priority thread
    init_work_queue();

    //initialize GPIOs on the board
    init_gpios();

    //handle the MCU interface (UART)
    err = mcu_intf_mgr_init();
    if (err) {
        LOG_ERR("Error initializing MCU Interface");
        error_idle_loop();
    }
    LOG_DBG("MCU Interface initialization complete");

    //handle the NFC reader interface (I2C)
    err = nfc_intf_mgr_init();
    if (err) {
        LOG_ERR("Error initializing NFC Interface");
        error_idle_loop();
    }
    LOG_DBG("NFC Interface initialization complete");

    k_sleep(K_SECONDS(1)); //allow debug to print

    //setup the BLE stack
    err = ble_mgr_init();
    if (err) {
        LOG_ERR("Error initializing BLE Manager");
        error_idle_loop();
    }
    LOG_DBG("BLE Manager initialization complete");
    k_sleep(K_SECONDS(1)); //allow debug to print

    LOG_DBG("Fall Monitor initialization complete, initialize state machine");
    state_machine_init();
}

/**
 * @}
 */
