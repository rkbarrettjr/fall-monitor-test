/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_work_queue work_queue.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief Worker Queue Utilities
 *
 * Configures and intializes a worker queue for use by components in the
 * Fall Monitor. The worker queue will allow high priority threads (e.g. interrupts)
 * to ofload processing to the worker queue's lower priority thread. Processing 
 * occurs FIFO in the worker queue.
 */
#include "work_queue_util.h"
#include <kernel.h>
#include <logging/log.h>
#include "state_machine.h"

#define WORKER_STACK_SIZE           1024                                            /**< size of worker queue stack */
#define WORKER_PRIORITY             5                                               /**< thread priority of worker queue */

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL); 

/** @brief Container for functions operating in the worker thread */
struct fm_state_machine_event_t {
    struct k_work           work;               /**< object required by worker queue */
    state_machine_evt_t     event;              /**< state machine event */
    void                    *data;              /**< additional data associated with the event */
};

#define MAX_NUM_FM_SM_ITEMS             3                                           /**< max number work queue items */

/** @brief Define the worker queue stack */
K_THREAD_STACK_DEFINE(m_work_q_stack_area, WORKER_STACK_SIZE);
static struct k_work_q                  m_work_q;                                   /**< workqueue */

static struct fm_state_machine_event_t  m_fm_sm_evt_list[MAX_NUM_FM_SM_ITEMS];      /**< pool of work queue items */     
static int                              m_fm_sm_index = 0;                          /**< next work queue item to use */

//local helper functions
static void fm_sm_worker(struct k_work *item);

/** @brief Work Queue Initialization
 */
void init_work_queue(void) {
    //setup workqueue to offload interrupts
    k_work_q_start(&m_work_q, m_work_q_stack_area, WORKER_STACK_SIZE, WORKER_PRIORITY);
    //callbacks for Fall Monitor state machine
    for (int i = 0; i < MAX_NUM_FM_SM_ITEMS; i++) {
        k_work_init(&m_fm_sm_evt_list[i].work, fm_sm_worker);
    }
}

/** @brief Submit State Machine Event
 * 
 * @details Callers handle an interrupt and generate a state machine event
 *          along with relevant data.
 * 
 * @param[in] event state machine event type
 * @param[in] data pointer to state machine data associated with the event
 */
void submit_state_machine_event(state_machine_evt_t event, void *data) {
    m_fm_sm_evt_list[m_fm_sm_index].event = event;
    m_fm_sm_evt_list[m_fm_sm_index].data = data;
    //offload task to the worker queue thread
    k_work_submit(&m_fm_sm_evt_list[m_fm_sm_index++].work);
    if (m_fm_sm_index >= MAX_NUM_FM_SM_ITEMS) {
        m_fm_sm_index = 0;
    }
}

//invoked from the worker queue, all events are handled here
static void fm_sm_worker(struct k_work *item) {
    struct fm_state_machine_event_t *evt_info = CONTAINER_OF(item, struct fm_state_machine_event_t, work);
    sm_handle_event(evt_info->event, evt_info->data);
}

/**
 * @}
 */