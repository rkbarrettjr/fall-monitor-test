/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_gpio_util gpio_util.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief GPIO Utilities
 *
 * Configures GPIOs to be used as interrupts on the device. Also allows
 * caller to read and set (output only GPIOs) the state of GPIOs.
 */
#include "gpio_util.h"
#include "logging/log.h"
#include "board_defines.h"

/** @brief Declare Logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL);

/** @brief Initialize GPIO Utilities
 *
 * @details Function will configure the device's GPIOs.
 * 
 * @return 0 on success
 */
int init_gpios(void) {
    int err = 1;
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    if (dev != NULL) {
        do {
            //disable the interrupts while they are being configured
            err = gpio_pin_interrupt_configure(dev, NFC_INT_PIN_IN, GPIO_INT_DISABLE);
            if (err) {
                LOG_ERR("Error disabling NFC interrupt");
                break;
            }
            err = gpio_pin_interrupt_configure(dev, MCU_INT_PIN_IN, GPIO_INT_DISABLE);
            if (err) {
                LOG_ERR("Error disabling MCU interrupt");
                break;
            }

            //configure GPIOs
            err = gpio_pin_configure(dev, NFC_INT_PIN_IN, (GPIO_INPUT | GPIO_ACTIVE_LOW | GPIO_PULL_UP));
            if (err) {
                LOG_ERR("Error configuring NFC pin");
                break;
            }
            err = gpio_pin_configure(dev, MCU_INT_PIN_IN, (GPIO_INPUT | GPIO_ACTIVE_LOW | GPIO_PULL_UP));
            if (err) {
                LOG_ERR("Error configuring MCU IN pin");
                break;
            }
            //set output on pin to be inactive (low is active)
            err = gpio_pin_configure(dev, MCU_INT_PIN_OUT, GPIO_OUTPUT_HIGH);
            if (err) {
                LOG_ERR("Error configuring MCU OUT pin");
                break;
            }       
        } while (0);    
    }
    return err;
}

/** @brief Configure Callback for GPIO Interrupt
 *
 * @details Function will configure the callback function that is invoked
 *          when the given GPIO pin is asserted.
 * 
 * @param[in] gpio_pin_num GPIO pin number on board
 * @param[in] data pointer to callback data
 * @param[in] callback function to invoke on GPIO assert
 * @return 0 on success
 */
int set_gpio_callback(int gpio_pin_num, struct gpio_callback *data, void *callback) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    gpio_init_callback(data, callback, BIT(gpio_pin_num));
    return gpio_add_callback(dev, data);
}

/** @brief Enable GPIO Interrupt
 *
 * @details Function will enable interrupts for the given GPIO.
 * 
 * @param[in] gpio_pin_num GPIO pin number on board
 * @return 0 on success
 */
int enable_gpio_interrupt(int gpio_pin_num) {
    int err = 1;
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    if (dev != NULL) {
        err = gpio_pin_interrupt_configure(dev, gpio_pin_num, GPIO_INT_EDGE_BOTH);
    }
    return err;
}

/** @brief Disable GPIO Interrupt
 *
 * @details Function will disable interrupts for the given GPIO.
 * 
 * @param[in] gpio_pin_num GPIO pin number on board
 * @return 0 on success
 */
int disable_gpio_interrupt(int gpio_pin_num) {
    int err = 1;
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    if (dev != NULL) {
        err = gpio_pin_interrupt_configure(dev, gpio_pin_num, GPIO_INT_DISABLE);
    }
    return err;
}

/** @brief Get GPIO State
 *
 * @details Function will return the state (0 or 1) of the given GPIO.
 * 
 * @param[in] gpio_pin_num GPIO pin number on board
 * @return state of the GPIO pin (0 or 1), -1 on error
 */
int get_gpio_state(int gpio_pin_num) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    if (dev == NULL) {
        return -1;
    }
    return gpio_pin_get(dev, gpio_pin_num);
}

/** @brief Set Ouput GPIO State
 *
 * @details Function will assert / de-assert the given GPIO.
 *
 * @param[in] gpio_pin_num pin number on the board
 * @param[in] state 1 to assert, 0 to de-assert
 */
void set_gpio_state(int gpio_pin_num, int state) {
    const struct device *dev = device_get_binding(GPIO_DEVICE_NAME);
    gpio_pin_set(dev, gpio_pin_num, state);
}

/**
 * @}
 */