/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_device_manager device_manager.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief Manager for BLE Connected Devices
 *
 * Manages devices that connect to the Fall Monitor via BLE. Events are provided to
 * the State Machine and it invokes the appropriate Device Manager handler. Individual 
 * state machines will be maintained for each device as it connects to the Fall Monitor.
 */
#include "device_manager.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <logging/log.h>
#include "ble_manager.h"
#include "board_defines.h"
#include "mcu_intf_manager.h"
#include "nfc_intf_manager.h"
#include "state_machine.h"

#define                                     MAX_NUM_DEVICES 5                   /**< maximum number of supported BLE peripherals */
device_info_t                               m_device_list[MAX_NUM_DEVICES];     /**< array of device information of connected peripherals */

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL);

//local functions
static device_info_t *find_device_by_name(const char *device_name);
static device_info_t *find_device_by_addr(const bt_addr_le_t *addr);
static device_info_t *create_dev_entry(char *device_name, bt_addr_le_t *addr);
static void init_device_info(device_info_t *info);
static void start_conn_timeout(device_info_t *info);
static void stop_conn_timeout(device_info_t *info);
static void disconnected_entry(device_info_t *info);
static void connected_entry(device_info_t *info);
static void connecting_entry(device_info_t *info);
static void update_mcu_connection_state(device_info_t *info, device_state_t state);

static void ble_conn_timer_handler(struct k_timer *dummy) {
    sm_create_event(BLE_CONN_TO_EVT, (device_info_t *)dummy->user_data);
}
K_TIMER_DEFINE(m_ble_conn_timer_1, ble_conn_timer_handler, NULL);                /**< BLE connection timeout handler 1 */
K_TIMER_DEFINE(m_ble_conn_timer_2, ble_conn_timer_handler, NULL);                /**< BLE connection timeout handler 2 */

/** @brief Device Manager Initialization
 */ 
void device_manager_init(void) {
    LOG_DBG("Device Manager initialization...");
    for (int i = 0; i < MAX_NUM_DEVICES; i++) {
        init_device_info(&m_device_list[i]);
    }
}

/** @brief Get Device BLE Address
 * 
 * @param[in] device_name name of device
 * @return BLE address if found, NULL otherwise
 */
const bt_addr_le_t *dm_get_device_addr(const char *device_name) {
    bt_addr_le_t *addr = NULL;
    device_info_t *info = find_device_by_name(device_name);
    if (info != NULL) {
        addr = &info->addr;
    }
    return addr;
}

/** @brief Get Device Information Entry
 * 
 * @details Function is used from BLE Manager to retrieve the device
 *          info entry when interrupts occur. The BLE Manager places
 *          the device info pointer in the payload of events generated.
 * 
 * @param[in] conn pointer to the BLE connection object
 * @return device info entry, NULL if not found
 */
device_info_t *dm_get_device_info(struct bt_conn *conn) {
    const bt_addr_le_t *addr = bt_conn_get_dst(conn);
    return find_device_by_addr(addr);
}

/** @brief BLE Connect Request 
 * 
 * @details Creates an DM entry for the device which maintains the state
 *          of the device while it is connecting/connected to the Fall Monitor.
 *          The device starts in the CONNECTING state where the BLE
 *          connect operation is attempted.
 * 
 * @param[in] device_name name of device (originates with NFC read)
 * @param[in] addr BLE address of the device
 */
void dm_connect_request(char *device_name, bt_addr_le_t *addr) {
    device_info_t *info = create_dev_entry(device_name, addr);
    if (info != NULL) {
        connecting_entry(info);
    }
}

/** @brief BLE Disonnect Request
 * 
 * @param[in] device_name name of device to disconnect
 */
void dm_disconnect_request(char *device_name) {
    LOG_DBG("Disconnect request made");
    device_info_t *info = find_device_by_name(device_name);
    if (info != NULL) {
        disconnected_entry(info);
    }
}

/** @brief BLE Disconnect All Devices Request
 */
void dm_disconnect_all_request(void) {
    for (int i = 0; i < MAX_NUM_DEVICES; i++) {
        //disconnect every device in Connected or Connecting states
        if (m_device_list[i].state != DISCONNECTED_DEV) {
            disconnected_entry(&m_device_list[i]);
        }
    }
}

/** @brief Handle BLE Connect from BLE Manager
 * 
 * @details Handler for the BLE connect event from the BLE stack.
 * 
 * @param[in] data pointer to the device info data associated with device
 */
void dm_handle_ble_connect(void *data) {
    device_info_t *info = (device_info_t *)data;
    if (info->state == CONNECTING_DEV) {
        connected_entry(info);
    } else {
        LOG_ERR("ERROR: Received BLE CONNECT in state: %d", info->state);
    }
}

/** @brief Handle BLE Disconnect from BLE Manager
 * 
 * @details Handler for the BLE disconnect event from the BLE stack.
 * 
 * @param[in] data pointer to the device info data associated with device
 */
void dm_handle_ble_disconnect(void *data) {
    device_info_t *info = (device_info_t *)data;
    if (data != NULL) {
        if (info->state == CONNECTED_DEV) {
            connecting_entry(info);
        } else {
            LOG_ERR("ERROR: Received BLE DISCONNECT in state: %d", info->state);
        }
    }
}

/** @brief Handle BLE Notification from BLE Manager
 * 
 * @details Handler for the BLE notification event from the BLE stack.
 * 
 * @param[in] data pointer to the device info data associated with device
 */
void dm_handle_ble_notification(void *data) {
    device_info_t *info = (device_info_t *)data;
    if (info->state == CONNECTED_DEV) {
        mcu_device_update_status(info->name, info->notification);
    } else {
        LOG_ERR("ERROR: Received BLE NOTIFICATION in state: %d", info->state);
    }
}

/** @brief Handle BLE Connection Timeout
 * 
 * @details Handler for timeout set for a BLE connection to complete
 * 
 * @param[in] data pointer to the device info data associated with device
 */
void dm_handle_ble_conn_timeout(void *data) {
    device_info_t *info = (device_info_t *)data;
    if (info->state == CONNECTING_DEV) {
        LOG_DBG("Device disconnected...");
        disconnected_entry(info);
    }
}

/** @brief Set Sensor State
 * 
 * @details Sets the sensor state (0/1) of the given device name.
 * 
 * @param[in] device_name name of sensor
 * @param[in] state 0 for open, 1 for closed
 */
void dm_set_sensor_state(const char *device_name, uint8_t state) {
    device_info_t *info = find_device_by_name(device_name);
    if (info != NULL) {
        info->sensor_state = state;
    }
}

/** @brief Get Sensor State
 * 
 * @details Gets the sensor state (0/1) of the given device name.
 * 
 * @param[in] device_name name of sensor
 * @return state 0 for open, 1 for closed
 */
uint8_t dm_get_sensor_state(const char *device_name) {
    uint8_t state = 0xFF;
    device_info_t *info = find_device_by_name(device_name);
    if (info != NULL) {
        state = info->sensor_state;
    }
    return state;
}

//finds the first unsed device etnry and returns it to the caller
static device_info_t *create_dev_entry(char *device_name, bt_addr_le_t *addr) {
    device_info_t *info = NULL;
    for (int i = 0; i < MAX_NUM_DEVICES; i++) {
        //if state is disconnected the entry is not currently in use
        if (m_device_list[i].state == DISCONNECTED_DEV) {
            info = &m_device_list[i];
            memcpy(info->name, device_name, DEVICE_NAME_LEN);
            memcpy(&info->addr, addr, sizeof(bt_addr_le_t));
            break;
        }
    }
    return info;
}

//returns information on the given device name if it exists
static device_info_t *find_device_by_name(const char *device_name) {
    device_info_t *info = NULL;
    for (int i = 0; i < MAX_NUM_DEVICES; i++) {
        if (!memcmp(device_name, m_device_list[i].name, DEVICE_NAME_LEN)) {
            info = &m_device_list[i];
            break;
        }
    }
    return info;
}

//returns information on the given device address if it exists
static device_info_t *find_device_by_addr(const bt_addr_le_t *addr) {
    device_info_t *info = NULL;
    for (int i = 0; i < MAX_NUM_DEVICES; i++) {
        if (!memcmp(addr, &m_device_list[i].addr, sizeof(bt_addr_le_t))) {
            info = &m_device_list[i];
            break;
        }
    }
    return info;
}

//disconnected entry state for the device state machine
static void disconnected_entry(device_info_t *info) {
    LOG_DBG("DISCONNECTED_DEV entry");
    info->state = DISCONNECTED_DEV;
    //disconnect device and inform the MCU
    ble_mgr_disconnect_device(&info->addr);
    update_mcu_connection_state(info, DISCONNECTED_DEV);
    //reset entry to make available for a future connection
    init_device_info(info);
}

//connected entry state for the device state machine
static void connected_entry(device_info_t *info) {
    LOG_DBG("CONNECTED_DEV entry");
    info->state = CONNECTED_DEV;
    //stop the connection timer and update the MCU
    stop_conn_timeout(info);
    update_mcu_connection_state(info, CONNECTED_DEV);
}

//connecting entry state for the device state machine
static void connecting_entry(device_info_t *info) {
    LOG_DBG("CONNECTING_DEV entry");
    info->state = CONNECTING_DEV;
    //start connection timeout and request BLE to connect
    start_conn_timeout(info);
    ble_mgr_connect_device(&info->addr);
}

//initalize a device info entry
static void init_device_info(device_info_t *info) {
    info->state = info->reported_state = DISCONNECTED_DEV;
    memset(info->name, 0, sizeof(DEVICE_NAME_LEN));
    memset(&info->addr, 0, sizeof(bt_addr_le_t));
    memset(info->notification, 0, sizeof(info->notification));
    info->sensor_state = 0xFF; //set sensor state to unknown value
}

static void start_conn_timeout(device_info_t *info) {
    //find unused timer and set for BLE connection timeout
    if (k_timer_remaining_get(&m_ble_conn_timer_1) == 0) {
        LOG_DBG("Using timer 1");
        m_ble_conn_timer_1.user_data = info;
        k_timer_start(&m_ble_conn_timer_1, K_SECONDS(BLE_CONN_TIMEOUT), K_SECONDS(0));
    } else if (k_timer_remaining_get(&m_ble_conn_timer_2) == 0) {
        LOG_DBG("Using timer 2");
        m_ble_conn_timer_2.user_data = info;
        k_timer_start(&m_ble_conn_timer_2, K_SECONDS(BLE_CONN_TIMEOUT), K_SECONDS(0));
    }
}

static void stop_conn_timeout(device_info_t *info) {
    device_info_t *timer_info1 = (device_info_t *)m_ble_conn_timer_1.user_data;
    device_info_t *timer_info2 = (device_info_t *)m_ble_conn_timer_2.user_data;
    if (!memcmp(info->name, timer_info1->name, DEVICE_NAME_LEN)) {
        k_timer_stop(&m_ble_conn_timer_1);
    } else if (!memcmp(info->name, timer_info2->name, DEVICE_NAME_LEN)) {
        k_timer_stop(&m_ble_conn_timer_2);
    }
}

//updates the device state to the MCU
static void update_mcu_connection_state(device_info_t *info, device_state_t state) {
    //only report if device changed state
    if (info->reported_state != state) {
        device_conn_status_t status = (state == CONNECTED_DEV) ? CONNECTED : NOT_CONNECTED;
        mcu_device_conn_status(info->name, status);
        info->reported_state = state;
    }
}

/**
 * @}
 */