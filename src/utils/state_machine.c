/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_state_machine state_machine.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief Fall Monitor State Machine
 *
 * State machine to drive the Fall Monitor behavior. Interrupts and timeouts processed here
 * are offloaded from the calling thread to the worker queue thread operating at a lower 
 * priority. Additionally the worker queue will process events FIFO ensuring events processing
 * by the state machine happen in the order they are provided.
 */
#include "state_machine.h"
#include "logging/log.h"
#include "ble_manager.h"
#include "board_defines.h"
#include "device_manager.h"
#include "mcu_intf_manager.h"
#include "nfc_intf_manager.h"
#include "work_queue_util.h"

/** @brief Enumeration of state machine states */
typedef enum { NFC_SCAN_STATE = 0, MCU_INFORM_STATE, BLE_SCAN_STATE, MAX_SM_STATE } state_machine_state_t;

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL); 

static state_machine_state_t                m_current_state;                    /**< current state of the machine */

static void ble_scan_timer_handler(struct k_timer *dummy) {
    sm_create_event(BLE_SCAN_TO_EVT, NULL);
}
K_TIMER_DEFINE(m_ble_scan_timer, ble_scan_timer_handler, NULL);                 /**< BLE scan timeout handler */

//local funcitons
static void nfc_scan_entry(void);
static void mcu_inform_entry(char *device_name);
static void ble_scan_entry(char *device_name);
static void handle_nfc_read(void *data);
static void handle_conn_allowed(void *data);
static void handle_conn_allowed_disco_existing(void *data);
static void handle_conn_not_allowed(void *data);
static void handle_ble_scan_to(void *data);
static void handle_ble_device_found(void *data);
static void handle_ble_connect_timeout(void *data);
static void handle_ble_connect(void *data);
static void handle_ble_disconnect(void *data);
static void handle_ble_notification(void *data);
static void handle_mcu_message(void *data);
static void handle_mcu_connect_device(void *data);

//  NFC_READ_EVT,           CONN_ALLOWED_EVT,           CONN_ALLOWED_DISCO_EXIST,               CONN_NOT_ALLOWED_EVT,
//  BLE_SCAN_TO_EVT,        BLE_DEV_FOUND_EVT,          BLE_CONN_TO_EVT,                        BLE_CONN_EVT,
//  BLE_DISCO_EVT,          BLE_NOTIF_EVT,              MCU_MSG_EVT,                            MCU_CONN_DEV
/** @brief table of event handlers - position dependent on the state_machine_evt_t enumeration */
void (*const event_table[MAX_STATE_MACHINE_EVT]) (void *) = {
    handle_nfc_read,        handle_conn_allowed,        handle_conn_allowed_disco_existing,     handle_conn_not_allowed,
    handle_ble_scan_to,     handle_ble_device_found,    handle_ble_connect_timeout,             handle_ble_connect,
    handle_ble_disconnect,  handle_ble_notification,    handle_mcu_message,                     handle_mcu_connect_device };

/** @brief State Machine Initialization
 */ 
void state_machine_init(void) {
    LOG_DBG("Initialize State Machine");

    //initial state after power-up
    nfc_scan_entry();
}

/** @brief Create State Machine Event
 * 
 * @details Create a state machine event attaching the given data. The
 *          processing of the event is offloaded to the worker queue operating
 *          in a lower priority thread than the interrupt that caused the event.
 * 
 *          Note that the data must be maintained outside the state machine
 *          while the event is being processed.
 * 
 * @param[in] event event type
 * @param[in] data pointer to data associated with the event
 */
void sm_create_event(state_machine_evt_t event, void *data) {
    submit_state_machine_event(event, data);
}

/** @brief Handle State Machine Event
 * 
 * @details Invoked from the worker queue to handle state machine events.
 * 
 * @param[in] event event type
 * @param[in] data pointer to data associated with the event
 */
void sm_handle_event(state_machine_evt_t event, void *data) {
    if (event >= 0 && event < MAX_STATE_MACHINE_EVT) {
        //invoke the handler corresponding to the event that occurred
        event_table[event](data);
    } else {
        LOG_ERR("ERROR: Unknown state machine event: %d", event);
    }
}

//NFC Scan state entry functions
static void nfc_scan_entry(void) {
    LOG_DBG("NFC_SCAN entry");
    m_current_state = NFC_SCAN_STATE;
    //BLE scanning disabled, NFC scanning enabled
    ble_mgr_stop_scan();
    nfc_start_scan();
}

//MCU Inform state entry functions
static void mcu_inform_entry(char *device_name) {
    LOG_DBG("MCU_INFORM entry");
    m_current_state = MCU_INFORM_STATE;
    //disable the NFC scan, request BLE scan for device from the MCU
    nfc_stop_scan();
    mcu_nfc_tag_read(device_name);
}

//BLE Scan state entry functions
static void ble_scan_entry(char *device_name) {
    LOG_DBG("BLE_SCAN entry");
    m_current_state = BLE_SCAN_STATE;
    //begin BLE scan, set timeout when scan will be stopped if device is not found
    ble_mgr_start_scan(device_name);
    k_timer_start(&m_ble_scan_timer, K_SECONDS(BLE_SCAN_TIMEOUT), K_SECONDS(0));

}

//handle when NFC reader indicates it has found a device
static void handle_nfc_read(void *data) {
    if (m_current_state == NFC_SCAN_STATE) {
        LOG_DBG("Handle NFC READ");
        //enter MCU Inform state
        mcu_inform_entry((char *)data);
    } else {
        LOG_ERR("ERROR: Received NFC READ in state: %d", m_current_state);
    }
}

//handle MCU connection allowed response
static void handle_conn_allowed(void *data) {
    if (m_current_state == MCU_INFORM_STATE) {
        LOG_DBG("Handle CONN ALLOWED");
        //start BLE scan for the device name
        ble_scan_entry((char *)data);
    } else {
        LOG_ERR("ERROR: Received CONNECT ALLOWED in state: %d", m_current_state);
    }
}

//handle MCU connection allowed with disconnect response
static void handle_conn_allowed_disco_existing(void *data) {
    if (m_current_state == MCU_INFORM_STATE) {
        LOG_DBG("Handle CONN ALLOWED DISCO EXIST");
        char *conn_device_name = (char *)data;
        char *disco_device_name = conn_device_name + DEVICE_NAME_LEN;
        dm_disconnect_request(disco_device_name);
        ble_scan_entry(conn_device_name);
    } else {
        LOG_ERR("ERROR: Received CONNECT ALLOWED DISCO EXIST in state: %d", m_current_state);
    }
}

//handle MCU connection not allowed response
static void handle_conn_not_allowed(void *data) {
    if (m_current_state == MCU_INFORM_STATE) {
        LOG_DBG("Handle CONN NOT ALLOWED");
        //go back to NFC scan
        nfc_scan_entry();
    } else {
        LOG_ERR("ERROR: Received CONNECT NOT ALLOWED in state: %d", m_current_state);
    }
}

//handle BLE scan timeout
static void handle_ble_scan_to(void *data) {
    if (m_current_state == BLE_SCAN_STATE) {
        LOG_DBG("Handle BLE SCAN T/O");
        //go back to NFC scan
        nfc_scan_entry();
    } else {
        LOG_ERR("ERROR: Received BLE SCAN T/O in state: %d", m_current_state);
    }
}

//handle BLE scan device found event
static void handle_ble_device_found(void *data) {
    if (m_current_state == BLE_SCAN_STATE) {
        LOG_DBG("Handle DEVICE FOUND");
        //disable the scan timeout
        k_timer_stop(&m_ble_scan_timer);
        //extract name and BLE address and request device manager to BLE connect
        ble_device_info_t *info = (ble_device_info_t *)data;
        dm_connect_request(info->device_name, &info->addr);
        //go back to NFC scan only, device manager will handle BLE device events
        nfc_scan_entry();
    } else {
        LOG_ERR("ERROR: Received BLE DEVICE FOUND: %d", m_current_state);
    }
}

//handle BLE connect timeout event - process in device manager
static void handle_ble_connect_timeout(void *data) {
    LOG_DBG("Handle BLE CONNECT T/O");
    dm_handle_ble_conn_timeout(data);
}

//handle BLE connect event - process in device manager
static void handle_ble_connect(void *data) {
    LOG_DBG("Handle BLE CONNECT");
    dm_handle_ble_connect(data);
}

//handle BLE disconnect event - process in device manager
static void handle_ble_disconnect(void *data) {
    LOG_DBG("Handle BLE DISCONNECT");
    dm_handle_ble_disconnect(data);
}

//handle BLE notification event
static void handle_ble_notification(void *data) {
    dm_handle_ble_notification(data);
}

//handle MCU message
static void handle_mcu_message(void *data) {
    mcu_message_received(data);
}

//handle MCU message to attempt a connect to the given device
static void handle_mcu_connect_device(void *data) {
    char *device_name = (char *)data;
    ble_scan_entry(device_name);
}

/**
 * @}
 */