/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_uart_comm uart_comm.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief UART Functionality
 *
 * This file contains functions that manage the UART bus
 * to the MCU. It will additionally manage the In/Out GPIOs
 * used by the NRF and MCU to indicate when transmitting may
 * occur.
 */
#include "uart_comm.h"
#include <drivers/uart.h>
#include "logging/log.h"
#include "board_defines.h"
#include "gpio_util.h"
#include "mcu_intf_manager.h"
#include "state_machine.h"

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL); 

#define                                     NUM_UART_BUFFERS 4                          /**< number of UART receive buffers */
#define                                     UART_BUF_SZ 64                              /**< UART receive buffer size */

static const struct device *                m_uart_dev;                                 /**< binding for the UART in Zephyr */                          
static struct gpio_callback                 m_cb_data;                                  /**< callback data for the GPIOs */
static uint8_t                              m_uart_buf[NUM_UART_BUFFERS][UART_BUF_SZ];  /**< pool of UART receive buffers */
static int                                  m_uart_index = 0;                           /**< current receive buffer being filled */
static int                                  m_rx_byte_num = 0;                          /**< position in current receive buffer */
static bool                                 m_uart_enabled = false;                     /**< tracks when UART is enabled/disabled */

//local helper functions
static void uart_read_cb(const struct device *dev, void *user_data);
static void mcu_in_int_gpio_cb(const struct device *dev, struct gpio_callback *gpio_cb, uint32_t pins);

/** @brief UART Initialization
 */ 
int uart_init(void) {
    LOG_DBG("Initializing UART");
    const struct uart_config cfg = {
        .baudrate = MCU_UART_BAUD_RATE,
        .parity = UART_CFG_PARITY_NONE,
        .stop_bits = UART_CFG_STOP_BITS_1,
        .data_bits = UART_CFG_DATA_BITS_8,
        .flow_ctrl = UART_CFG_FLOW_CTRL_NONE
    };

    m_uart_dev = device_get_binding(UART_DEVICE_NAME);
    if (m_uart_dev == NULL) {
        LOG_ERR("Error getting UART binding");
        return -1;
    }

    int err = uart_configure(m_uart_dev, &cfg);
    if (!err) {
        uart_irq_callback_set(m_uart_dev, uart_read_cb);
        uart_irq_rx_enable(m_uart_dev);
    } else {
        LOG_ERR("Error confgiuring UART");
    }

    if (!err) {
        //configure the MCU input pin callback and enable the interrupt
        err = set_gpio_callback(MCU_INT_PIN_IN, &m_cb_data, mcu_in_int_gpio_cb);
        if (!err) {
            err = enable_gpio_interrupt(MCU_INT_PIN_IN);
            if (!err) {
                //get the intial state
                int val = get_gpio_state(MCU_INT_PIN_IN);
                LOG_DBG("MCU Input GPIO initial state: %d", val);
                //TODO ??
            }
        }
    }
    return err;
}

/** @brief Request UART Communications Start
 * 
 * @brief Caller has data to send via the UART. The MCU will initiate
 *        communication by sending an ENQ message after which the
 *        caller may send a message to the MCU.
 */ 
void uart_req_to_send(void) {
    //assert (active low) GPIO out to indicate request to send
    set_gpio_state(MCU_INT_PIN_OUT, 0);
}

/** @brief Request UART Communications Stop
 */ 
void uart_send_complete(void) {
    //de-assert (active low) GPIO out to indicate sending is complete
    set_gpio_state(MCU_INT_PIN_OUT, 1);   
}

/** @brief Send Message via UART
 * 
 * @param[in] data data to send
 * @param[in] len length of data being sent
 * @return 0 on success
 */ 
int uart_send(char *data, size_t len) {
    for (int i = 0; i < len; i++) {
        uart_poll_out(m_uart_dev, data[i]);
    }
    if (len > 1) { //TODO remove
        data[len] = '\0';
        //printk("UART send: %s\n", data);
        LOG_DBG("UART send: %s", log_strdup(data));
    } else {
        //printk("UART send: 0x%02x\n", data[0]);
        LOG_DBG("UART send: 0x%02x", data[0]);
    }
    return 0;
}

//callback from UART interrupt invoked when data available from UART
static void uart_read_cb(const struct device *dev, void *user_data) {
    uart_irq_update(dev);
    if (uart_irq_rx_ready(dev)) {
        char read_char;
        while (uart_poll_in(dev, &read_char) == 0) {
            m_uart_buf[m_uart_index][m_rx_byte_num++] = read_char;
            if (is_termination_character(read_char)) {
                //generate event with received message
                sm_create_event(MCU_MSG_EVT, m_uart_buf[m_uart_index]);
                //get next buffer
                m_uart_index = (m_uart_index + 1) % NUM_UART_BUFFERS;
                memset(m_uart_buf[m_uart_index], 0, UART_BUF_SZ);
            }
        }
    }
}



//callback when MCU input GPIO changes state
static void mcu_in_int_gpio_cb(const struct device *dev, struct gpio_callback *gpio_cb, uint32_t pins) {
    //get logical GPIO state
    int val = get_gpio_state(MCU_INT_PIN_IN);
    if (val == 0 && m_uart_enabled) {
        LOG_DBG("UART IN GPIO de-assert");
        //TODO disable UART
        m_uart_enabled = false;
    } else if (val == 1 && !m_uart_enabled) {
        LOG_DBG("UART IN GPIO asserted");
        //TODO enable UART
        m_uart_enabled = true;
        set_gpio_state(MCU_INT_PIN_OUT, 0); //active low
    }
}

/**
 * @}
 */

