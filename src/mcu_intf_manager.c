/** @file
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 *
 * @defgroup soc_fm_nrf_mcu_intf_manager mcu_intf_manager.c
 * @{
 * @ingroup soc_fm_nrf
 * @brief MCU Interface Manager
 *
 * This file contains functions that managesending/receiving
 * messages from the MCU. It includes handling the interface to the
 * UART as well as the message protocol between the MCU and nRF.
 */
#include "mcu_intf_manager.h"
#include <stdbool.h>
#include "logging/log.h"
#include "device_manager.h"
#include "uart_comm.h"
#include "state_machine.h"

/** @brief Enumeration of message buffer states */
typedef enum { UNUSED_BUF = 0, READY, SENDING } buf_state_t;

/** @brief Declare logger */
LOG_MODULE_DECLARE(FallMonNrf, CONFIG_LOG_MAX_LEVEL);

#define                 NRF_EVENT               0x49                        /**< NRF event message type */
#define                 NFC_TAG_READ            0x30                        /**< NFC tag read sub-type */
#define                 DEV_CONN_STATUS         0x31                        /**< BLE device connection status sub-type */
#define                 DEV_STATE_STATUS        0x32                        /**< BLE device status (notification) sub-type */

#define                 STX                     0x02                        /**< message start delimiter */
#define                 ETX                     0x03                        /**< message end delimiter */
#define                 EOT                     0x04                        /**< no more events to send */
#define                 ENQ                     0x05                        /**< inquire if events to send (TX by MCU) */
#define                 ACK                     0x06                        /**< ack successful receipt */
#define                 NAK                     0x15                        /**< message not received successfully */

#define                 STM_CMD                 0x43                        /**< STM command message type */
#define                 CONN_DEV                0x33                        /**< connect to device sub-type */
#define                 CONN_DEV_SZ             22                          /**< size of connection allow/disallow msg from MCU */
#define                 CONN_NFC_DEV            0x34                        /**< connect to NFC device sub-type */
#define                 CONN_DISC_NFC_DEV       0x35                        /**< connect to NFC device / disconnect device sub-type */
#define                 CONN_DISC_NFC_DEV_SZ    38                          /**< size of conn w/ disco msg from MCU */
#define                 IGNORE_DEV              0x36                        /**< ignore device sub-type */
#define                 DISCO_ALL_DEVS          0x37                        /**< disconnect all sensors sub-type */
#define                 DISCO_ALL_DEVS_SZ       6                           /**< size of disconnect all devs msg from MCU */
#define                 NCA_RELAY_STATE         0x38                        /**< set NCA relay state sub-type */
#define                 NCA_RELAY_STATE_SZ      23                          /**< size of NCA relay state msg from MCU */

#define                 NUM_MSG_BUFFERS         8                           /**< number outgoing MCU message buffers */
#define                 MSG_BUF_SIZE            64                          /**< size of outgoing MCU message buffer */

/** @brief Structure used to manage messages being sent to the MCU */
typedef struct {
    buf_state_t         state;                      //state of the buffer
    uint8_t             buffer[MSG_BUF_SIZE];       //data
    uint8_t             buf_len;                    //length of data in buffer
} send_msg_buf_t;

static send_msg_buf_t   m_msg_buffers[NUM_MSG_BUFFERS];                     /**< pool of outgoing MCU message buffers */
static uint8_t          m_msg_buf_index;                                    /**< location in message buffer pool */
static uint8_t          m_msg_buf_send_index;                               /**< MCU message awaiting an ACK */
static uint8_t          m_rsp_msg;                                          /**< memory for one byte MCU response messages */

//local functions
static uint8_t calc_lrc(uint8_t *payload, uint8_t payload_len);
static bool check_lrc(uint8_t *msg, uint8_t msg_len);
static uint8_t ascii_to_hex (char ascii);
static void set_hex_digit(uint8_t digit, uint8_t* dest);
static void create_message(uint8_t type, uint8_t sub_type, uint8_t *payload, uint8_t payload_len, send_msg_buf_t *msg_buffer);
static send_msg_buf_t *get_msg_buf(void);
static int process_mcu_msg(uint8_t *msg);

/** @brief MCU Interface Initialization
 */ 
int mcu_intf_mgr_init(void) {
    LOG_DBG("Begin MCU Interface initialization...");

    //set up a pool of message buffers
    for (int i = 0; i < NUM_MSG_BUFFERS; i++) {
        memset(&m_msg_buffers[i], 0, sizeof(send_msg_buf_t));
    }
    m_msg_buf_index = m_msg_buf_send_index = 0;

    int err = uart_init();
    if (err) {
        LOG_ERR("Error initializing UART");
    }
    return err;
}

/** @brief Send NFC Tag Read Request
 * 
 * @brief Verifies the device name read via NFC is valid and then makes
 *        a request to the MCU to determine if device may connect.
 * 
 * @param[in] device_name pointer to name of device read via NFC
 */ 
void mcu_nfc_tag_read(char *device_name) {
    //request a buffer to build the MCU message
    send_msg_buf_t *msg_buffer = get_msg_buf();
    if (msg_buffer != NULL) {
        create_message(NRF_EVENT, NFC_TAG_READ, device_name, DEVICE_NAME_LEN, msg_buffer);
        uart_req_to_send();
    }
}

/** @brief Send Device Connect Status
 * 
 * @brief Update the MCU with the connect status of the given device.
 * 
 * @param[in] device_name pointer to name of device
 * @param[in] status connected or disconnected
 */ 
void mcu_device_conn_status(char *device_name, device_conn_status_t status) {
    uint8_t payload[DEVICE_NAME_LEN + 2];
    memcpy(payload, device_name, DEVICE_NAME_LEN);
    set_hex_digit(status, payload + DEVICE_NAME_LEN);
    send_msg_buf_t *msg_buffer = get_msg_buf();
    if (msg_buffer != NULL) {
        create_message(NRF_EVENT, DEV_CONN_STATUS, payload, DEVICE_NAME_LEN + 2, msg_buffer);
        uart_req_to_send();
    }
}
/** @brief Send Device Status Update
 * 
 * @brief Update the MCU with the status of the given device. The status
 *        originates from a BLE notification.
 * 
 * @param[in] device_name pointer to name of device
 * @param[in] status payload of the BLE notification
 */ 
void mcu_device_update_status(char *device_name, uint8_t *status) {
    uint8_t last_state = dm_get_sensor_state(device_name);
    if (status[3] == 0x42 && last_state != status[0]) { //TODO - format status msg
        dm_set_sensor_state(device_name, status[0]);
        uint8_t payload[DEVICE_NAME_LEN + 8];
        memcpy(payload, device_name, DEVICE_NAME_LEN);

        //set status in the message reversed to correct endian-ness
        for (int i = 0; i < 4; i++) {
            set_hex_digit(status[3 -i], payload + DEVICE_NAME_LEN + i * 2);
        }
        send_msg_buf_t *msg_buffer = get_msg_buf();
        if (msg_buffer != NULL) {
            create_message(NRF_EVENT, DEV_STATE_STATUS, payload, DEVICE_NAME_LEN + 8, msg_buffer);
            uart_req_to_send();
        }
    }
}

/** @brief MCU Termination Character
 * 
 * @param[in] val MCU character
 * @return true if termination character
 */ 
bool is_termination_character(uint8_t val) {
    bool result = false;
    if (val == ETX || val == ENQ || val == ACK || val == NAK) {
        result = true;
    }
    return result;
}

/** @brief MCU Message Receive
 * 
 * @param[in] data data received from the MCU
 */ 
void mcu_message_received(void *data) {
    uint8_t *msg = (uint8_t *)data;
    int err;
    switch (msg[0]) {
        case ENQ:
            LOG_DBG("Rx MCU ENQ message");
            //if msg buffer is in READY state then send it
            if (m_msg_buffers[m_msg_buf_send_index].state == READY) {
                m_msg_buffers[m_msg_buf_send_index].state = SENDING;
                err = uart_send(m_msg_buffers[m_msg_buf_send_index].buffer, 
                    m_msg_buffers[m_msg_buf_send_index].buf_len);
                if (err) {
                    LOG_ERR("Error sending msg on ENQ");
                }
            } else {
                LOG_DBG("Send EOT after ENQ");
                //no messages ready, send EOT
                m_rsp_msg = EOT;
                err = uart_send(&m_rsp_msg, 1);
                if (err) {
                    LOG_ERR("Error sending EOT");
                }
                uart_send_complete();
            }
            break;
        case ACK:
            LOG_DBG("Rx MCU ACK message");
            //set the ack'd buffer to unused and increment send index
            memset(&m_msg_buffers[m_msg_buf_send_index], 0, sizeof(send_msg_buf_t));
            m_msg_buf_index = (m_msg_buf_index + 1) % NUM_MSG_BUFFERS;
            //if next buffer is in READY state then send it
            if (m_msg_buffers[m_msg_buf_send_index].state == READY) {
                m_msg_buffers[m_msg_buf_send_index].state = SENDING;
                err = uart_send(m_msg_buffers[m_msg_buf_send_index].buffer, 
                    m_msg_buffers[m_msg_buf_send_index].buf_len);
                if (err) {
                    LOG_ERR("Error sending msg on ACK");
                }
            } else {
                LOG_DBG("Send EOT after ACK");
                //no messages ready, send EOT
                m_rsp_msg = EOT;
                err = uart_send(&m_rsp_msg, 1);
                if (err) {
                    LOG_ERR("Error sending EOT");
                }
                uart_send_complete();
            }
            break;
        case NAK:
            LOG_DBG("Rx MCU NAK message");
            //last sent msg was nack'd, resend it
            err = uart_send(m_msg_buffers[m_msg_buf_send_index].buffer, 
                m_msg_buffers[m_msg_buf_send_index].buf_len);
            if (err) {
                LOG_ERR("Error sending msg on NACK");
            }
            break;
        case STX:
            //got the start delimiter of a message, process it
            err = process_mcu_msg(msg);
            if (!err) {
                m_rsp_msg = ACK; //good receive
            } else {
                m_rsp_msg = NAK; //bad receive
            }
            err = uart_send(&m_rsp_msg, 1);
            if (err) {
                LOG_ERR("Error sending MCU msg ACK/NAK");
            }
            break;
        default:
            LOG_WRN("Rx unknown MCU message: 0x%02x", msg[0]);
                m_rsp_msg = NAK;
                err = uart_send(&m_rsp_msg, 1);
                if (err) {
                    LOG_ERR("Error sending NAK");
                }
            break;
    }
}

//create MCU message
static void create_message(uint8_t type, uint8_t sub_type, uint8_t *payload, uint8_t payload_len, send_msg_buf_t *msg_buffer) {
    uint8_t index = 0;
    //create the MCU message
    uint8_t *buf = msg_buffer->buffer;
    buf[index++] = STX;
    buf[index++] = type;
    buf[index++] = sub_type;
    for (int i = 0; i < payload_len; i++) {
        buf[index++] = payload[i];
    }
    //exclude leading byte in LRC calculation
    uint8_t lrc = calc_lrc(buf + 1, index - 1);
    set_hex_digit(lrc, buf + index);
    index += 2;
    buf[index++] = ETX;

    //update the length and the state
    msg_buffer->buf_len = index;
    msg_buffer->state = READY;
}

//process the given MCU command message
static int process_mcu_msg(uint8_t *msg) {
    int err = 1;
    if (msg[1] == STM_CMD) {
        uint8_t subtype = msg[2];
        uint8_t *payload = &msg[3];
        switch (subtype) {
            case CONN_DEV: //connect device request from MCU
                if (check_lrc(msg, CONN_DEV_SZ)) {
                    LOG_DBG("MCU Connect request");
                    //generate connect request with the device name
                    sm_create_event(MCU_CONN_DEV, payload);
                    err = 0;
                }
            case CONN_NFC_DEV: //allow connect of device read via NFC
                if (check_lrc(msg, CONN_DEV_SZ)) {
                    LOG_DBG("MCU Connect Allow");
                    //generate connection allowed for the device name
                    sm_create_event(CONN_ALLOWED_EVT, payload);
                    err = 0;
                }
                break;
            case CONN_DISC_NFC_DEV: //allow connect w/ disconnect of device
                if (check_lrc(msg, CONN_DISC_NFC_DEV_SZ)) {
                    LOG_DBG("MCU Conn/Disco Allow");
                    //generate disco/conn for the two devices
                    sm_create_event(CONN_ALLOWED_DISCO_EXIST, payload);
                    err = 0;
                }
                break;
            case IGNORE_DEV: //do not allow connect
                if (check_lrc(msg, CONN_DEV_SZ)) {
                    LOG_DBG("MCU Conn Not Allowed");
                    //ignore the given device
                    sm_create_event(IGNORE_DEV, payload);
                    err = 0;
                }
                break;
            case DISCO_ALL_DEVS:
                if (check_lrc(msg, DISCO_ALL_DEVS_SZ)) {
                    LOG_DBG("MCU Disco All Devs");
                    dm_disconnect_all_request();
                    err = 0;
                }
                break;
            case NCA_RELAY_STATE:
                if (check_lrc(msg, NCA_RELAY_STATE_SZ)) {
                    LOG_DBG("MCU NCA Relay State");
                    //TODO send command to NCA
                    err = 0;
                }
                break;
            default:
                LOG_ERR("Unknown MCU subtype: %d", subtype);
                err = 2;
                break;
        }
    }
    return err;
}

//calculate Longitudinal Reduncancy Check for outgoing message
static uint8_t calc_lrc(uint8_t *payload, uint8_t payload_len) {
    uint8_t lrc = 0x00;
    for (int i = 0; i < payload_len; i++) {
        lrc ^= payload[i];
    }
    return lrc;
}

//verify Lognitudinal Redundancy Check for incoming message
static bool check_lrc(uint8_t *msg, uint8_t msg_len) {
    return true; //TODO

    uint8_t lrc = 0xFF;
    if ((0x02 == msg[0]) && (0x03 == msg[msg_len - 1])) {
        lrc = ascii_to_hex(msg[msg_len - 3]) << 4;
        lrc |= ascii_to_hex(msg[msg_len - 2]);
        for (int i = 1; i < (msg_len - 4); i++) {
            lrc ^= msg[i];
        }
    }
    return (lrc == 0x00 ? true : false);
}

static uint8_t ascii_to_hex (char ascii) {
    return (uint8_t) ((ascii <= '9') ? (ascii - 0x30) : ((ascii & 0x0F) + 0x09));
}

static const char HEX_ASCII_TABLE[] = "0123456789ABCDEF";
static void set_hex_digit(uint8_t digit, uint8_t* dest) {
    *dest++ = HEX_ASCII_TABLE[(digit >> 4) & 0xF];
    *dest = HEX_ASCII_TABLE[digit & 0xF];
}

static send_msg_buf_t *get_msg_buf(void) {
    send_msg_buf_t *buf = NULL;
    if (m_msg_buffers[m_msg_buf_index].state != UNUSED_BUF) {
        LOG_ERR("Out of MCU message buffers"); //TODO??
    } else {
        buf = &m_msg_buffers[m_msg_buf_index];
        m_msg_buf_index = (m_msg_buf_index + 1) % NUM_MSG_BUFFERS;
    }
    return buf;
}

/**
 * @}
 */