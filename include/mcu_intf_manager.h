/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */

#ifndef MCU_INTF_MANAGER_H
#define MCU_INTF_MANAGER_H

#include <stdbool.h>
#include <stdint.h>
#include "nfc_intf_manager.h"

/** @brief Device connection status */
typedef enum { NOT_CONNECTED = 0, CONNECTED } device_conn_status_t;

int mcu_intf_mgr_init(void);
void mcu_nfc_tag_read(char *device_name);
void mcu_device_conn_status(char *device_name, device_conn_status_t status);
void mcu_device_update_status(char *device_name, uint8_t *status);
void mcu_message_received(void *data);
bool is_termination_character(uint8_t val);

#endif