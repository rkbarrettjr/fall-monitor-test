/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */
#ifndef WORK_QUEUE_UTIL_H
#define WORK_QUEUE_UTIL_H

#include "state_machine.h"

void init_work_queue(void);
void submit_state_machine_event(state_machine_evt_t event, void *data);

#endif