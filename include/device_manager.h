/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */
#ifndef DEVICE_MANAGER_H
#define DEVICE_MANAGER_H

#include <bluetooth/bluetooth.h>
#include "nfc_intf_manager.h"

/** @brief States of a BLE device */
typedef enum { DISCONNECTED_DEV = 0, CONNECTING_DEV, CONNECTED_DEV } device_state_t;

typedef struct {
    device_state_t              state;                      /**< device state */
    device_state_t              reported_state;             /**< last device state reported to the MCU */
    char                        name[DEVICE_NAME_LEN];      /**< device name - originally from NFC read */
    bt_addr_le_t                addr;                       /**< BT address of device */
    char                        notification[8];            /**< buffer for BLE notification data */
    uint8_t                     sensor_state;               /**< state of the chair sensor, not applicable for NCA */
} device_info_t;

void device_manager_init(void);
void dm_connect_request(char *device_name, bt_addr_le_t *addr);
void dm_disconnect_request(char *device_name);
void dm_disconnect_all_request(void);

void dm_handle_ble_conn_timeout(void *data);
void dm_handle_ble_connect(void *data);
void dm_handle_ble_disconnect(void *data);
void dm_handle_ble_notification(void *data);

device_info_t *dm_get_device_info(struct bt_conn *conn);
const bt_addr_le_t *dm_get_device_addr(const char *device_name);
void dm_set_sensor_state(const char *device_name, uint8_t state);
uint8_t dm_get_sensor_state(const char *device_name);

#endif