/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */
#ifndef GPIO_UTIL_H
#define GPIO_UTIL_H

#include <drivers/gpio.h>

int     init_gpios(void);
int     set_gpio_callback(int gpion_pin_num, struct gpio_callback *data, void *cb);
int     enable_gpio_interrupt(int gpio_pin_num);
int     disable_gpio_interrupt(int gpio_pin_num);
int     get_gpio_state(int gpio_pin_num);
void    set_gpio_state(int gpio_pin_num, int state);

#endif