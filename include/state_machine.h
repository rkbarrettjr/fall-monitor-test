/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */
#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H


/** @brief Events for the state machine */
typedef enum {  NONE_STATE_MACHINE_EVT = -1,
                NFC_READ_EVT = 0,       CONN_ALLOWED_EVT,   CONN_ALLOWED_DISCO_EXIST,   CONN_NOT_ALLOWED_EVT,
                BLE_SCAN_TO_EVT,        BLE_DEV_FOUND_EVT,  BLE_CONN_TO_EVT,            BLE_CONN_EVT,
                BLE_DISCO_EVT,          BLE_NOTIF_EVT,      MCU_MSG_EVT,                MCU_CONN_DEV,
                MAX_STATE_MACHINE_EVT } state_machine_evt_t;

void state_machine_init(void);
void sm_create_event(state_machine_evt_t event, void *data);
void sm_handle_event(state_machine_evt_t event, void *data);

#endif