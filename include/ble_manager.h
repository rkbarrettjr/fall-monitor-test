/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */
#ifndef BLE_MANAGER_H
#define BLE_MANGER_H

#include "nfc_intf_manager.h"
#include <bluetooth/bluetooth.h>

typedef struct {
	char 	        	device_name[DEVICE_NAME_LEN];
	bt_addr_le_t 		addr;
} ble_device_info_t;

int ble_mgr_init(void);
int ble_mgr_start_scan(char *device_name);
int ble_mgr_stop_scan(void);
int ble_mgr_connect_device(bt_addr_le_t *ble_addr);
int ble_mgr_disconnect_device(bt_addr_le_t *ble_addr);
int ble_write_device(char *device_name, uint32_t data);
int ble_read_device(char *device_name);

#endif
