/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */

#ifndef BOARD_DEFINES_H
#define BOARD_DEFINES_H

//GPIO defines
#define                             GPIO_DEVICE_NAME        "GPIO_0"        /**< GPIO device name for binding */
#define                             SENSOR_PIN_IN           3               /**< Sensor GPIO pin number */
#define                             SENSOR_PIN_OUT          2               /**< Sensor GPIO pullup pin number */

//I2C defines
#define                             I2C_DEVICE_NAME         "I2C_0"         /**< I2C device name for binding */
#define                             NFC_I2C_ADDR            8               /**< Address of NFC device on the I2C */
#define                             NFC_I2C_SDA             26              /**< Data pin for NFC I2C bus */
#define                             NFC_I2C_SCL             27              /**< Clock pin for NFC I2C bus */
#define                             NFC_INT_PIN_IN          28              /**< Input interrupt for NFC to indicate data available */

//UART defines
#define                             UART_DEVICE_NAME        "UART_0"        /**< UART device name for binding */
#define                             MCU_UART_BAUD_RATE      115200          /**< UART baud rate */
#define                             MCU_UART_TX             6               /**< Transmit pin for MCU UART */
#define                             MCU_UART_RX             8               /**< Receive pin for MCU UART */
#define                             MCU_INT_PIN_OUT         30              /**< Output interrupt to manage UART communication */
#define                             MCU_INT_PIN_IN          31              /**< Input interrupt to manage UART communication */

#define                             BLE_SCAN_TIMEOUT        25              /**< */
#define                             BLE_CONN_TIMEOUT        10              /**< */

#endif