#ifndef UART_COMM_H
#define UART_COMM_H

#include <stddef.h>

int uart_init(void);
void uart_req_to_send(void);
void uart_send_complete(void);
int uart_send(char *data, size_t len);

#endif