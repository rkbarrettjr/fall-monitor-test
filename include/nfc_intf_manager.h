/*
 * Copyright (c) 2021, Bold Type
 * All rights reserved.
 */

#ifndef NFC_INTF_MANAGER_H
#define NFC_INTF_MANAGER_H

#include <stdbool.h>

#define DEVICE_NAME_LEN 16

int nfc_intf_mgr_init(void);
void nfc_start_scan(void);
void nfc_stop_scan(void);

void simulate_nfc_read(int num);

#endif