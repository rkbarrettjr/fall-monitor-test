# Sitter on Cue Fall Monitor BLE Firmware #

Repo contains firmware for the NRF52832 chip operating on the SoC Fall Monitor.

### What does this firmware do? ###
* Communicates with the MCU (STM32) via a UART interface
* Communicates with an NFC Reader (CLRC66303) via an I2C interface
* The NFC reader will read a tag containing the BLE advertised name of a Sensor
* The firmware will scan for the Sensor and connect when requested
* Received BLE notifications will be forwarded to the MCU as they are received

### Repo and Target Setup ###

* Target: TIDI Soc Fall Monitor (nRF52832, STM32, CLRC66303)
* Project created using Visual Studio Code and PlatformIO extension
* Zephyr RTOS

### Build and Deployment Procedure

* This procedure is to be performed using macOS Big Sur V11.1
* Install VS Code
* Open VS Code and from Extensions install PlatformIO

### Who do I talk to? ###

* Kent Barrett
* kbarrett@boldtype.com
